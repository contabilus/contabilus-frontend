# ContabilUS Frontend

# How to start developing

Follow this commands in this order:

### `cp .env.example .env.development.local`

### `yarn` to install dependencies

### `yarn start` to run project

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
